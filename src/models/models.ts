/**
 * Used to display the various pages on the home page
 */
export interface HomeItem {
  title: string;
  icon: string;
  link: string;
  isVisible: boolean; // allows the user to determine which features are shwown on the home screen.
  forStudentOnly: boolean; // userd to determine whether to display a home item for a non-user student
}

/**
 * For Contact page and provider
 */
export interface Contact {
  title: string;
  number: string;
  ext: string;
  faculty?: string;
}

/**
 * Used for the Parking page and provider
 */
export interface ParkingLot {
  name: string;
  img_url: string;
  maxCapacity?: number;
  availableSpaces?: number; // possible future feature: use geofencing to detect when someone is in one of the lots and send an action notif to confirm that they are there
  coords: Coords;
}

/**
 * Used by the map page ad by pages who pass
 * locations to the map page
 */
export interface Location {
  name: string;
  coords: Coords;
  img_url?: string;
  alias?: string[];
  faculty?: 'SciTech' | 'SoSci' | 'MedSci' | 'MSBM' | 'Law' | 'Humanities' | 'Eng' | 'Nursing' | 'Other';
  feature: 'classroom' | 'bathroom' | 'parking' | 'office' | 'food' | 'atm' | 'bus stop' | 'other';
  level?: string; // is it on the second floor?
}

export interface Coords {
  lat: number;
  lng: number;
}

export class Route {
  origin: string;
  destination: string;
  time: string;

  constructor(origin: string, destination: string, time: string) {
    this.origin = origin;
    this.destination = destination;
    this.time = time;
  }
}

export class BusRoute {
  title: string;
  routes: Route[];
  tags: string[];
  active: boolean;
  color: string;

  constructor(title: string, routes: string[][], tags: string[], active: boolean) {
    this.title = title;
    for(let x = 0; x < routes.length; x++) {
      this.routes.push(new Route(routes[x][0], routes[x][1], routes[x][2]));
    }
    this.tags = tags;
    this.active = active;
  }

  toString() {
    let rString = '';
    for(let x = 0; x < this.routes.length; x++) {
      rString = rString + this.routes[x].origin + "-->" + this.routes[x].destination + this.routes[x].time + "\n";
    }
    return this.title + "\n" + rString;
  }
}



