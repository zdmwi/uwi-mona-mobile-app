export interface User {
  IDNUMBER: string;
  FIRSTNAME: string;
  LASTNAME: string;
  STUDENT_LEVEL: string;
  FULL_PART_STATUS: string;
  FACULTY: string;
  DEGREE: string;
  MAJOR: string;
  TERM: string;
}
