export interface Event {
  nid: string;
  title: string;
  start_date: string;
  end_date: string;
  faculty?: string;
  event_image: string | false;
  url: string;
}
