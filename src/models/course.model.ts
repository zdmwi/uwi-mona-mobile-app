export interface CourseSegment {
  CRN: string;
  TERM: string;
  COURSE_CODE: string;
  COURSE_TITLE: string;
  SEQ: string;
  TYPE: string;
  INSTRUCTOR: string;
  CATEGORY: string;
  DAY: string;
  START_TIME: string;
  END_TIME: string;
  BUILDING: string;
  ROOM_CODE: string;
  ROOM: string;
}

export interface Course {
  COURSE_TITLE: string;
  COURSE_CODE: string;
  INSTRUCTOR?: string;
  CRN?: string;
}
