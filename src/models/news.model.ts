export interface NewsItem {
  nid: string;
  title: string;
  posted_on: string;
  faculty?: string;
  article_image: string | false;
  url: string;
}
