export interface Intersection {
  out: number;
  entry: boolean[];
  bearings: number[];
  location: number[];
  in?: number;
}

export interface Maneuver {
  bearing_after: number;
  location: number[];
  bearing_before: number;
  type: string;
  modifier: string;
}

export interface Step {
  intersections: Intersection[];
  driving_side: string;
  geometry: string;
  mode: string;
  duration: number;
  maneuver: Maneuver;
  weight: number;
  distance: number;
  name: string;
}

export interface Leg {
  steps: Step[];
  distance: number;
  duration: number;
  summary: string;
  weight: number;
}

export interface Route {
  geometry: string;
  legs: Leg[];
  distance: number;
  duration: number;
  weight_name: string;
  weight: number;
}

export interface Waypoint {
    hint: string;
    name: string;
    location: number[];
}

export interface RoutingResponse {
  code: string;
  routes: Route[];
  waypoints: Waypoint[];
}
