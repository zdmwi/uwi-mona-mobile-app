import { Component } from '@angular/core';

import { Platform, NavController, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';
import { User } from 'src/models/user.model';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'List',
      url: '/list',
      icon: 'list'
    }
  ];

  user: User;

  constructor(
    private platform: Platform,
    private navCtrl: NavController,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authService: AuthService,
    private userService: UserService,
    private menu: MenuController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleBlackTranslucent();
      this.splashScreen.hide();

      this.authService.authState.subscribe(state => {
        if (state) {
          this.navCtrl.navigateRoot('home');
        } else {
          this.navCtrl.navigateRoot('auth');
        }
      });
    });
  }

  isAuthenticated() {
    return this.authService.isAuthenticated();
  }

  getUserId() {
    return this.authService.getId();
  }

  getUser() {
    return this.authService.getUser();
  }

  getUserType() {
    return this.authService.isVisitor() ? 'Visitor' : 'Student';
  }

  getUserPhoto() {
    return this.userService.getUserPhoto();
  }

  logout() {
    this.menu.close().then(() => {
      this.authService.logout();
    });
  }
}
