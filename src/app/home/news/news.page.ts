import { Component, OnInit } from '@angular/core';
import { NewsService } from './../../services/news.service';
import { NewsItem } from './../../../models/news.model';
import { FACULTIES } from './../../../data/faculties';

@Component({
  selector: 'app-news',
  templateUrl: './news.page.html',
  styleUrls: ['./news.page.scss'],
})
export class NewsPage {

  newsItems: NewsItem[];
  selectedFaculty;
  faculties = FACULTIES;

  constructor(
    private newsService: NewsService
  ) { }

  ionViewDidEnter() {
    setTimeout(()=> {
      this.newsService.getAllNews()
      .subscribe((resp: NewsItem[]) => {
          this.newsItems = resp;
          console.log(resp);
        });
    }, 1500);
  }

  getNewsItems(event: any) {
    this.newsService.getAllNews()
    .subscribe((resp: NewsItem[]) => {
      this.newsItems = resp;
      event.target.complete();
    });
  }

  openLink(url: string) {
    return window.open(url, '_blank');
  }

}
