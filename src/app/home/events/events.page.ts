import { Component, OnInit } from '@angular/core';
import { NewsService } from './../../services/news.service';
import { Event } from './../../../models/event.model';
import { FACULTIES } from './../../../data/faculties';

@Component({
  selector: 'app-events',
  templateUrl: './events.page.html',
  styleUrls: ['./events.page.scss'],
})
export class EventsPage implements OnInit {

  events: Event[];
  faculties = FACULTIES;
  selectedFaculty: string;

  constructor(
    private newsService: NewsService
  ) { }

  ngOnInit() {
    setTimeout(()=> {
      this.newsService.getAllEvents()
      .subscribe((resp: Event[]) => {
        console.log('events', resp);
        this.events = resp;
      });
    }, 1000);
  }

  getEvents(event: any) {
    this.newsService.getAllEvents()
    .subscribe((resp: Event[]) => {
      this.events = resp;
      event.target.complete();
    });
  }

  openLink(url: string) {
    return window.open(url, '_blank');
  }

}
