import { Component, OnInit } from '@angular/core';
import { timer, Observable } from 'rxjs';
import { ClockService } from 'src/app/services/clock.service';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
})
export class CalendarComponent implements OnInit {
  private displayDate: string;
  private dayOfWeek: string;
  private calendarDayDate: number;
  private daysOfWeek: string[] = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  private daysThisCalendarWeek: string[];
  
  constructor(
    private clockService: ClockService
  ) {}

  ngOnInit() {
    this.clockService.getClock().subscribe((date: Date) => {
      this.dayOfWeek = this.getDayOfWeek(date);
      this.calendarDayDate = this.getCalendarDayDate(date);
      this.daysThisCalendarWeek = this.getCalendarWeek(date);
      this.displayDate = this.getDisplayDate(date);
    });
  }

  private getDisplayDate(date: Date): string {
    const months = ['January', 'February', 'March', 'April', 'May', 'June',
                    'July', 'August', 'September', 'October', 'November',
                    'December'];

    const date_str = `${months[date.getMonth()]} ${this.getOrdinalNum(this.getCalendarDayDate(date))}, ${this.getCalendarYear(date)}`; 
    return date_str;
  }

  private getDayOfWeek(date: Date): string {
    return this.daysOfWeek[date.getDay()];
  }

  private getCalendarYear(date: Date): number {
    return date.getFullYear();
  }

  private getCalendarDayDate(date: Date): number {
    return date.getDate();
  }

  private getCalendarWeek(date: Date): string[] {
    const current = new Date(date.getTime());
    const week= new Array(); 
    // set the date to Sunday
    current.setDate((current.getDate() - current.getDay()));
    for (var i = 0; i < 7; i++) {
        week.push(new Date(current).getDate().toString().padStart(2, '0')); 
        current.setDate(current.getDate() + 1);
    }
    return week; 
  }

  private getOrdinalNum(n: number): string {
    return n + (n > 0 ? ['th', 'st', 'nd', 'rd'][(n > 3 && n < 21) || n % 10 > 3 ? 0 : n % 10] : '');
  }

}
