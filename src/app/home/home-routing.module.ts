import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: HomePage
      },
      {
        path: 'map',
        loadChildren: './map/map.module#MapPageModule'
      },
      {
        path: 'parking',
        loadChildren: './parking/parking.module#ParkingPageModule'
      },
      {
        path: 'restaurants',
        children: [
          {
            path: '',
            loadChildren: './restaurants/restaurants.module#RestaurantsPageModule'
          },
          // {
          //   path: ':restaurantId',
          //   loadChildren: './restaurants/restaurant/restaurant.module#RestaurantPageModule'
          // }
        ]
      },
      {
        path: 'busses',
        children: [
          {
            path: '',
            loadChildren: './busses/busses.module#BussesPageModule'
          },
          // {
          //   path: ':busId',
          //   loadChildren: './busses/bus-route/bus-route.module#BusRouteModule'
          // }
        ]
      },
      {
        path: 'directory',
        loadChildren: './directory/directory.module#DirectoryPageModule'
      },
      {
        path: 'schedule',
        children: [
          {
            path: '',
            loadChildren: './schedule/schedule.module#SchedulePageModule'
          },
          {
            path: ':courseTitle',
            loadChildren: './course-details/course-details.module#CourseDetailsPageModule'
          }
        ]
      },
      { path: 'news-and-events', loadChildren: './news-and-events/news-and-events.module#NewsAndEventsPageModule' },
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {}
