import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directory',
  templateUrl: './directory.page.html',
  styleUrls: ['./directory.page.scss'],
})
export class DirectoryPage implements OnInit {

  categories = [
    'Science and Technology',
    'Medical Sciences', 'Law', 'Social Sciences', 'MSBM', 'Nursing',
    'General'
  ];

  constructor() { }

  ngOnInit() {
  }

}
