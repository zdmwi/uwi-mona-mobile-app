import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsAndEventsPage } from './news-and-events.page';

describe('NewsAndEventsPage', () => {
  let component: NewsAndEventsPage;
  let fixture: ComponentFixture<NewsAndEventsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsAndEventsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsAndEventsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
