import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NewsAndEventsPage } from './news-and-events.page';

const routes: Routes = [
  {
    path: '',
    component: NewsAndEventsPage,
    children: [
      {
        path: 'news',
        loadChildren: './../news/news.module#NewsPageModule'
      },
      {
        path: 'events',
        loadChildren: './../events/events.module#EventsPageModule'
      },
      {
        path: '',
        redirectTo: '/home/news-and-events/news',
        pathMatch: 'full'
      }

    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [NewsAndEventsPage]
})
export class NewsAndEventsPageModule {}
