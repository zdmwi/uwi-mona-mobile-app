import { Component, OnInit } from '@angular/core';
import { LocationService } from './../../services/location.service';
import { ParkingLot } from './../../../models/models';
import { Router } from '@angular/router';

@Component({
  selector: 'app-parking',
  templateUrl: './parking.page.html',
  styleUrls: ['./parking.page.scss'],
})
export class ParkingPage implements OnInit {

  lots: ParkingLot[];

  constructor(
    private locationService: LocationService,
    private router: Router
  ) { }

  ngOnInit() {
    this.lots = this.locationService.getParkingLots();
  }

  onLocate(name: string) {
    const loc: ParkingLot = this.lots.find(l => l.name === name);
    this.locationService.passedLocation = {
      name: loc.name,
      img_url: loc.img_url,
      coords: loc.coords,
      feature: 'parking'
    };
    this.router.navigateByUrl('/home/map');
  }

}
