import { Component, OnInit } from '@angular/core';
import { ScheduleService } from './../../services/schedule.service';
import { CourseSegment } from './../../../models/course.model';
import { SegmentChangeEventDetail } from '@ionic/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.page.html',
  styleUrls: ['./schedule.page.scss'],
})
export class SchedulePage implements OnInit {

  segments: CourseSegment[];
  selectedSegments: CourseSegment[];
  days = [ 'All', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  hours: string[];
  selectedDay: string;
  courses: string[] = [];
  today: string;

  constructor(
    private scheduleService: ScheduleService,
    private router: Router
  ) { }

  ngOnInit() {
    // this.hours = this.createHourlyIntervals();
    this.scheduleService.getSchedule()
    .subscribe((resp: CourseSegment[]) => {
      this.segments = resp;
      this.segments.forEach(s => {
        if (!this.courses.includes(s.COURSE_TITLE)) {
          this.courses.push(s.COURSE_TITLE);
        }
      });
      this.today = this.numToDay(new Date().getDay());

      this.selectedDay = this.today;
      this.selectedSegments = this.segments.filter(s => s.DAY === this.selectedDay);
    });
  }

  private numToDay(num: number): string {
    switch (num) {
      case 1:
        return 'Monday';
      case 2:
        return 'Tuesday';
      case 3:
        return 'Wednesday';
      case 4:
        return 'Thursday';
      case 5:
        return 'Friday';
      case 6:
        return 'Saturday';
    }
  }

  private compareSections(a: CourseSegment, b: CourseSegment) {
    if (a.START_TIME > b.START_TIME) {
      return 1;
    } else if (b.START_TIME > a.START_TIME) {
      return -1;
    }
    return 0;
  }

  onSelectDay(ev: CustomEvent<SegmentChangeEventDetail>) {
    const {value} = ev.detail;
    console.log(`${value} selected`);
    this.selectedDay = value;
    const unsortedSegments: CourseSegment[] = this.segments.filter(s => {
      return s.DAY === value;
    });

    this.selectedSegments = unsortedSegments.sort(this.compareSections);
  }

  toCourseDesc(courseTitle: string) {
    console.log(courseTitle);
    this.router.navigateByUrl('/home/schedule/' + courseTitle);
  }

  // createHourlyIntervals() {
  //   let from: any = "08:00";
  //   let until: any = "21:00";
  //   //"01/01/2001" is just an arbitrary date
  //   until = Date.parse("01/01/2001 " + until);
  //   from = Date.parse("01/01/2001 " + from);
  //   const max = (Math.abs(until - from) / (60 * 60 * 1000));
  //   const time = new Date(from);
  //   const hours = [];
  //   for (let i = 0; i <= max; i++) {
  //       //doubleZeros just adds a zero in front of the value if it's smaller than 10.
  //       const hour = this.doubleZeros(time.getHours());
  //       const minute = this.doubleZeros(time.getMinutes());
  //       hours.push(hour+":"+minute);
  //       time.setMinutes(time.getMinutes() + 60);
  //   }
  //   return hours;
  // }

  // filter(list: CourseSegment[]) {
  //   list.sort((a, b) => {
  //     return a.START_TIME < b.START_TIME ? 1 : -1;
  //   });
  // }

  doubleZeros(num: number) {
    if (num < 10) {
      return `0${num}`;
    } else {
      return num.toString();
    }
  }

}
