import { Component } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { HomeItem } from 'src/models/models';
import { HOME_ITEMS } from '../../data/home-items';
import { NewsItem } from 'src/models/news.model';
import { NewsService } from '../services/news.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  navItems: HomeItem[];
  articles: NewsItem[];

  slideOpts = {
    slidesPerView: 1.2,
    spaceBetween: 0,
    centeredSlides: true,
    initialSlide: 1,
    autoplay: {
      delay: 4000,
      disableOnInteraction: false,
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
  };

  constructor(
    private authService: AuthService,
    private newsService: NewsService,
    private menu: MenuController
  ) {}

  ionViewDidEnter() {
    // filter the available options to visitors to avoid showing them useless options
    this.navItems = !this.isVisitor() ? HOME_ITEMS : HOME_ITEMS.filter(item => !item.forStudentOnly);

    this.newsService.getAllNews().subscribe(articles => {
      this.articles = articles.slice(0, 5);
    });
  }

  openMenu() {
    this.menu.enable(true);
    this.menu.open();
  }

  isVisitor(): boolean {
    return this.authService.isVisitor();
  }

  getUserPhoto() {
    return `https://ext-web-srvs.uwimona.edu.jm/sas/api-test/badge/${this.authService.getId()}`
  }
}
