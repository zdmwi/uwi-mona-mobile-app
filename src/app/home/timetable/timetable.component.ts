import { Component, OnInit, ViewChildren, QueryList, ElementRef,  Renderer2, ViewChild, AfterViewChecked } from '@angular/core';
import { ScheduleService } from 'src/app/services/schedule.service';
import { CourseSegment } from 'src/models/course.model';
import { ClockService } from 'src/app/services/clock.service';
import { LocalNotifications, ELocalNotificationTriggerUnit } from '@ionic-native/local-notifications/ngx';

@Component({
  selector: 'app-timetable',
  templateUrl: './timetable.component.html',
  styleUrls: ['./timetable.component.scss'],
})
export class TimetableComponent implements OnInit, AfterViewChecked {
  @ViewChild('courseBlockContainer') courseBlockContainer: ElementRef;
  @ViewChildren('courseBlock') courseBlocks: QueryList<ElementRef>

  private displayTime: string;
  private currentTime: string;
  private todaysCourses: CourseSegment[] = [];
  private courseSchedule: object = {};
  private courseBlocksResized: boolean = false;
  private daysOfWeek: string[] = [
    'Sunday', 'Monday', 'Tuesday', 'Wednesday', 
    'Thursday', 'Friday', 'Saturday'
  ];

  private CLASS_NOTIF_TIME = 15; // in minutes
  private HOUR_BLOCK_DIFFERENCE = 59;
  private scale = 3;

  constructor(
    private scheduleService: ScheduleService,
    private clockService: ClockService,
    private renderer: Renderer2,
    private localNotifications: LocalNotifications
  ) { }

  ngOnInit() {
    // generate a list of times in the day
    const allTimes = [];

    for (let hr = 0; hr < 24; hr++) {
      // create 1 hour blocks of time of the format HH:MM
      let startTime: string = `${hr}:00`;
      let endTime: string = `${hr}:59`;

      this.courseSchedule[startTime] = {
        START_TIME: startTime,
        // we use course code here as the display text in the timetable
        // so if the slot is empty it will just have the time of the slot
        COURSE_CODE: this.tConvert(startTime.padStart(5, '0')),
        COURSE_TITLE: null,
        END_TIME: endTime
      };
    }
    
    this.scheduleService.getSchedule().subscribe((courses: CourseSegment[]) => {
      for (let course of this.getTodaysCourses(new Date(2020, 1, 20), courses)) {
        // get all hours between
        const numHoursBetween = this.getNumHoursBetween(
          course.START_TIME, course.END_TIME);

        this.courseSchedule[course.START_TIME] = course;

        if (numHoursBetween > 0) {
          const times = Object.keys(this.courseSchedule);
          const idx = times.indexOf(course.START_TIME);
          
          const inner = idx + 1;
          const hoursToRemove = times.slice(inner, inner + numHoursBetween);
          
          for (let hr of hoursToRemove) {
            delete this.courseSchedule[hr];
          }
        } 
      }
      console.log(this.courseSchedule);
      this.todaysCourses = Object.values(this.courseSchedule);
    });

    this.clockService.getClock().subscribe((date: Date) => {
      this.displayTime = date.toLocaleString('en-US', 
        { 
          hour: 'numeric', 
          minute: 'numeric',
          hour12: true 
        }
      );

      this.currentTime = this.getCurrentTime(date);

      // send a push notificaiton if there is a class at the current time
      // make sure the notification is sent exactly on the hour i.e. when
      // there are 0 seconds past the hour. This stops notifications from
      // blasting constantly.

      if (date.getSeconds() == 0 && 
          this.courseSchedule[this.currentTime] != null &&
          this.courseSchedule[this.currentTime].COURSE_TITLE != null) {

            console.log('now');
            const code = this.courseSchedule[this.currentTime].COURSE_CODE;
            const title = this.courseSchedule[this.currentTime].COURSE_TITLE;

            this.sendNotif("It's time for class!",
              `${code}: ${title} has started.`);
          }

      // send a push notification if there is a class in
      // CLASS_NOTIF_TIME minutes
      // add CLASS_NOTIF_TIME minutes to the current time
      const lookAhead = new Date(date.getTime());
      lookAhead.setMinutes(lookAhead.getMinutes() + this.CLASS_NOTIF_TIME);
      const lookAheadStr = this.getCurrentTime(lookAhead);

      if (date.getSeconds() == 0 &&
          this.courseSchedule[lookAheadStr] != null &&
          this.courseSchedule[lookAheadStr].COURSE_TITLE != null) {
          
            console.log('in 15');
            const code = this.courseSchedule[lookAheadStr].COURSE_CODE;
            const title = this.courseSchedule[lookAheadStr].COURSE_TITLE;

            this.sendNotif(`Class is about to start!`, 
            `${code}: ${title} starts in ${this.CLASS_NOTIF_TIME} minutes.`);
      }

      this.renderer.setProperty(
        this.courseBlockContainer.nativeElement,
        'scrollLeft',
        this.getTimetableScrollPosition(date)
      );
    });
  }

  ngAfterViewChecked() {
    if (this.courseBlocks.length && !this.courseBlocksResized) {
      this.courseBlocksResized = true;

      // calculate the width of each course block
      const widths = this.todaysCourses.map(course => {
        const endTime = this.normalizeEndTime(course.END_TIME);
        let difference = (endTime - this.formatToMilitaryTime(course.START_TIME));
        if (difference > this.HOUR_BLOCK_DIFFERENCE) {
          const multiplier = ((difference - this.HOUR_BLOCK_DIFFERENCE) / 100) + 1;
          difference = this.HOUR_BLOCK_DIFFERENCE * multiplier;
        }
        const width = difference * this.scale;
        return width;
      });
      this.courseBlocks.forEach((courseBlock, index) => {
        this.renderer.setStyle(
          courseBlock.nativeElement,
          'min-width',
          `${widths[index]}px`
        );
      });

      this.renderer.setProperty(
        this.courseBlockContainer.nativeElement,
        'scrollLeft',
        this.getTimetableScrollPosition(new Date())
      );
    }
  }

  private sendNotif(title: string, text: string): void {
    this.localNotifications.schedule({
      id: 1,
      title,
      text,
      trigger: { in: 3, unit: ELocalNotificationTriggerUnit.SECOND },
      foreground: true
    });
  }

  private getNumHoursBetween(startTime: string, endTime: string): number {
    const t1 = this.formatToMilitaryTime(startTime);
    const t2 = this.formatToMilitaryTime(endTime) + 41;

    return ((t2 - t1) / 100) - 1;
  }

  private normalizeEndTime(time: string): number {
    let endTime = this.formatToMilitaryTime(time);
    /* 
      because the blank courses are being converted directly to numbers
      by removing the colons between the times there's always a difference
      of 41. 
      eg. 9:00 to 9:59 becomes 900 to 959 but
          9:00 to 10:00 becomes 900 to 1000 event though the hour block ends at 9:59 as well
      So to normalize the time difference based width we have to 
      subtract that magic number.
    */ 
    const tfHourDifference = 41;
    if (endTime % 10 === 0) {
      endTime -= tfHourDifference;
    }
    return endTime;
  }

  private formatToMilitaryTime(time: string): number {
    let timeStr = '';
    if (time) {
      timeStr = time.split(':').join('');
    }
    return parseInt(timeStr);
  }

  private getCurrentTime(date: Date): string {
    const hours = date.getHours();
    const minutes = date.getMinutes().toString().padStart(2, '0');
    return `${hours}:${minutes}`;
  }

  private getDayOfWeek(date: Date): string {
    return this.daysOfWeek[date.getDay()];
  }

  private getTodaysCourses(date: Date, courses: CourseSegment[]): CourseSegment[] {
    return courses.filter(course => {
      return course.DAY === this.getDayOfWeek(date);
    });
  }

  private isBetweenHours(startTime: string, endTime: string): boolean {
    const t1 = this.formatToMilitaryTime(startTime);
    const t2 = this.normalizeEndTime(endTime);
    const current = this.formatToMilitaryTime(this.currentTime);
    return current >= t1 && current <= t2;
  }

  private getTimetableScrollPosition(date: Date): number {
    const currentTime = date.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: false});
    
    const scaledTime = this.scaleTime(this.formatToMilitaryTime(currentTime));

    const hourBlockDifference = this.HOUR_BLOCK_DIFFERENCE;
    return (scaledTime) * hourBlockDifference * this.scale;
  }

  private scaleTime(time: number): number {
    const ratio = 100 / 60;

    // get the part of the number after the decimal point
    const fraction = (time % 100) / 100;
    const whole = Math.floor(time / 100);

    const scaledFraction = fraction * ratio;
    return whole + scaledFraction;
  }

  private tConvert (time: any): string {
    // Check correct time format and split into components
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
  
    if (time.length > 1) { // If time format correct
      time = time.slice (1);  // Remove full string match value
      time[5] = + time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
      time[0] = + time[0] % 12 || 12; // Adjust hours
    }
    return time.join (''); // return adjusted time or original string
  }
}
