import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Map, map, tileLayer, marker } from 'leaflet';
import { Location, Coords } from './../../../models/models';
import * as L from 'leaflet';
import { LocationService } from './../../services/location.service';
import { ToastController, Platform } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage {

  // @ViewChild('map') mapContainer: ElementRef;
  UWI_COORDINATES: L.LatLngExpression = [18.006045, -76.746828];

  map: Map;
  currentLocation: Coords;
  locations: Location[];
  watchId: number;

  passedLoc: Location;
  parkingLayer: L.FeatureGroup = L.featureGroup();
  foodLayer: L.FeatureGroup = L.featureGroup();
  bathroomLayer: L.FeatureGroup = L.featureGroup();
  busLayer: L.FeatureGroup = L.featureGroup();

  currentPosLayer: L.FeatureGroup = L.featureGroup();
  resultsLayer: L.FeatureGroup = L.featureGroup();

  myIcon: L.DivIcon = L.divIcon({
    iconSize: [16, 16],
    iconAnchor: [8, 8],
    className: 'my-icon',
    html: '<div class="gps-ring"></div>'
  });

  constructor(
    private locationService: LocationService,
    private toastCtrl: ToastController,
    private geolocation: Geolocation,
    private plt: Platform
  ) {}

  ionViewDidEnter() {
    this.leafletMap();
    this.locations = this.locationService.getLocations();
    this.loadMarkerLayers();
    this.showLayers();
    this.getPassedLocation();
    this.watchLocation();
  }

  leafletMap() {
    // In setView add latLng and zoom
    this.map = L.map('mapId').setView(this.UWI_COORDINATES, 17);
    tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      // tslint:disable-next-line: max-line-length
      attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
      maxZoom: 24,
      minZoom:  13
    }).addTo(this.map);


    // L.marker([28.6, 77]).addTo(this.map)
    //   .bindPopup('Ionic 4 <br> Leaflet.')
    //   .openPopup();
    this.map.on('click', () => this.resultsLayer.clearLayers());
  }

  /** Remove map when we have multiple map object */
  ionViewWillLeave() {
    this.map.remove();
  }

  private addToFeatureGroup(location: Location, group: L.FeatureGroup): void {
    const {lat, lng} = location.coords;
    L.marker([lat, lng]).addTo(group).bindPopup(location.name);
  }

  private showLayers() {
    this.resultsLayer.addTo(this.map);
    this.currentPosLayer.addTo(this.map);
    console.log(this.map.hasLayer(this.currentPosLayer));
    console.log(this.currentPosLayer.getLayers().length);
  }

  private loadMarkerLayers(): void {
    let points: Location[];
    points = this.locations.filter(pt => pt.feature === 'parking');
    points.forEach(pt => this.addToFeatureGroup(pt, this.parkingLayer));

    points = this.locations.filter(pt => pt.feature === 'bathroom');
    points.forEach(pt => this.addToFeatureGroup(pt, this.bathroomLayer));

    points = this.locations.filter(pt => pt.feature === 'food');
    points.forEach(pt => this.addToFeatureGroup(pt, this.foodLayer));

    points = this.locations.filter(pt => pt.feature === 'bus stop');
    points.forEach(pt => this.addToFeatureGroup(pt, this.busLayer));
  }

  loadMarkers(type: number) {
    switch (type) {
      case 1:
        this.toggleMarkers(this.parkingLayer);
        break;
      case 2:
        this.toggleMarkers(this.bathroomLayer);
        break;
      case 3:
        this.toggleMarkers(this.foodLayer);
        break;
      case 4:
        this.toggleMarkers(this.busLayer);
        break;

      default:
        break;
    }
  }

  getPassedLocation(): boolean {
    this.passedLoc = this.locationService.passedLocation;
    if (this.passedLoc) {
      this.makeMarker(this.passedLoc);
      console.log('got passed location');
      return true;
    }
    return false;

  }

  makeMarker(location: Location, popUp?: string) {
    const {lat, lng} = location.coords;
    popUp = popUp || location.name;

    // L.marker([lat, lng]).addTo(this.map).bindPopup(popUp).openPopup();
    const markr = L.marker([lat, lng]);
    this.resultsLayer.addLayer(markr);
    markr.bindPopup(popUp).openPopup();
    console.log('created marker');


  }

  toggleMarkers(fGroup: L.FeatureGroup) {
    if (this.map.hasLayer(fGroup)) {
      this.map.removeLayer(fGroup);
    } else {
      fGroup.addTo(this.map);
    }
  }

  locateMe(): void {
    if (this.currentLocation !== null) {
      this.map.panTo(this.currentLocation);
      console.log(this.currentPosLayer.getLayers().length);
    } else {
      this.notify('Current Location is unavailable at this time');
    }
  }

  private async notify(msg: string, time: number = 2000) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: time
    });

    toast.present();
  }

  watchLocation() {
    this.plt.ready().then( () => {
      const watch = this.geolocation.watchPosition();
      watch.subscribe(
        data => {
          const {latitude, longitude} = data.coords;
          this.currentLocation = {lat: latitude, lng: longitude};
          const you = L.marker([latitude, longitude], {icon: this.myIcon});
          if (this.currentPosLayer.getLayers().length) {
            this.currentPosLayer.clearLayers();
          }
          this.currentPosLayer.addLayer(you);
        },
        err => {
          this.notify('there was a problem with getting your location');
        }
      );

    });
  }



}
