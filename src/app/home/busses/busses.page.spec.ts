import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BussesPage } from './busses.page';

describe('BussesPage', () => {
  let component: BussesPage;
  let fixture: ComponentFixture<BussesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BussesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BussesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
