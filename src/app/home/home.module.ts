import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';

import { HomePage } from './home.page';
import { HomeRoutingModule } from './home-routing.module';

import { CalendarComponent } from './calendar/calendar.component';
import { TimetableComponent } from './timetable/timetable.component';

@NgModule({
  imports: [
  CommonModule,
    FormsModule,
    IonicModule,
    HomeRoutingModule
  ],
  declarations: [HomePage, CalendarComponent, TimetableComponent],
  bootstrap: [CalendarComponent, TimetableComponent]
})
export class HomePageModule {}
