import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { User } from '../../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) {}

  getUserPhoto() {
    return `https://ext-web-srvs.uwimona.edu.jm/sas/api-test/badge/${this.authService.getId()}`
  }
}
