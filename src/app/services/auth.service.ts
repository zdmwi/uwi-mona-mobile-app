import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavController, ToastController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { BehaviorSubject } from 'rxjs';
import { User } from '../../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authState = new BehaviorSubject(false);
  _user: User;
  _id: string;
  _isVisitor: boolean = false;

  url = 'https://ext-web-srvs.uwimona.edu.jm/sas/api-test/ad/authenticate';

  constructor(
    private http: HttpClient,
    private storage: Storage,
    private platform: Platform,
    private navCtrl: NavController,
    private toast: ToastController
  ) {
    this.platform.ready().then(() => {
      this.ifLoggedIn();
    });
  }

  ifLoggedIn() {
    this.storage.get('user').then(response => {
      if (response) {
        this.authState.next(true);
        this._id = response.id;
        this._user = response.user;
      }
    });
  }

  isAuthenticated(): boolean {
    return this.authState.value;
  }

  isVisitor(): boolean {
    return this._isVisitor;
  }

  getId() {
    return this._id;
  }

  getUser(): User {
    return this._user;
  }

  login(id: string, password: string) {
    this.http.post(this.url, {
      id,
      password
    }).subscribe(
      resp => {
        this.http
          .get<User>(`https://ext-web-srvs.uwimona.edu.jm/sas/api-test/registration/enrollment/${id}/basic`)
          .subscribe((user: User) => {
            this._user = user;
            this.storage.set('user', {id: id, user: user}).then(response => {
              this._id = id;
              this.navCtrl.navigateForward('/home');
              this.authState.next(true);
            });
          });
      },
      err => {
        this.showMessage('Id number or password is incorrect');
      });
  }

  loginVisitor() {
    this._isVisitor = true;
    this.authState.next(true);
  }

  logout() {
    this.storage.remove('user').then(() => {
      this._id = '';
      this._user = null;
      this._isVisitor = false;
      this.navCtrl.navigateBack('auth');
      this.authState.next(false);
    });
  }

  async showMessage(message: string) {
    const tst = await this.toast.create({
      message,
      duration: 2000,
      position: 'top'
    });

    tst.present();
  }
}
