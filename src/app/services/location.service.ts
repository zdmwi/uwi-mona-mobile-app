import { Injectable } from '@angular/core';
import { Location, ParkingLot } from 'src/models/models';
import { PARKING_LOTS } from './../../data/parking-lots';
import { LOCATIONS } from './../../data/locations';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor() { }

  // tslint:disable-next-line: variable-name
  private _passedLocation: Location;

  get passedLocation() {
    return this._passedLocation;
  }

  set passedLocation(location: Location) {
    this._passedLocation = location;
  }

  getParkingLots(): ParkingLot[] {
    return PARKING_LOTS;
  }

  getLocations(): Location[] {
    return LOCATIONS;
  }

  getFoodPlaces() {}




}
