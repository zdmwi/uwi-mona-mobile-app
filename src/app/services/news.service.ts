import { Injectable } from '@angular/core';
import { NewsItem } from 'src/models/news.model';
import { Event } from 'src/models/event.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  private newsUrl =  'https://www.mona.uwi.edu/marcom/api/news';
  private eventUrl =  'https://www.mona.uwi.edu/marcom/api/events';

  constructor(
    private http: HttpClient
  ) { }

  getAllNews(): Observable<NewsItem[]> {
    return this.http.get<NewsItem[]>(this.newsUrl);
  }

  getNewsByFaculty(facultyCode: string): Observable<NewsItem[]> {
    return this.http.get<NewsItem[]>(`${this.newsUrl}?filter[faculty]=${facultyCode}`);
  }

  getAllEvents(): Observable<Event[]> {
    return this.http.get<Event[]>(this.eventUrl);
  }

  getEventsByFaculty(facultyCode: string): Observable<Event[]> {
    return this.http.get<Event[]>(`${this.eventUrl}?filter[faculty]=${facultyCode}`);
  }

}
