import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { CourseSegment } from './../../models/course.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  url = 'https://ext-web-srvs.uwimona.edu.jm/sas/api-test/registration/courses';

  constructor(
    private http: HttpClient,
    private auth: AuthService
    ) { }

  getSchedule(): Observable<CourseSegment[]> {
    return this.http.get<CourseSegment[]>(`${this.url}/${this.auth.getId()}/schedule`);
  }
}
