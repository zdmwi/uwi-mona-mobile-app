export const FACULTIES = [
  {
    name: 'Science & Technology',
    code: 'PA'
  },
  {
    name: 'Engineering',
    code: 'EN'
  },
  {
    name: 'Social Sciences',
    code: 'SS'
  },
  {
    name: 'Law',
    code: 'LW'
  },
  {
    name: 'Mesical Sciences',
    code: 'MD'
  },
  {
    name: 'Humanities & Education',
    code: 'PA'
  },
  {
    name: 'Sport',
    code: 'SP'
  },
];
