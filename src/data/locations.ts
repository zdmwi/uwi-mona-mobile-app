import { Location } from '../models/models';

export const LOCATIONS: Location[] = [
  {
    name: 'By SciTech',
    faculty: 'SciTech',
    feature: 'parking',
    coords: {
      lat: 18.003667,
      lng: -76.750292
    }
  },
  {
    name: 'By Juici',
    faculty: 'SciTech',
    feature: 'parking',
    coords: {
      lat: 18.005524182513227,
      lng: -76.74867779016495
    }
  },
  {
    name: 'Main Parking Lot',
    feature: 'parking',
    coords: {
      lat: 18.00211621071818,
      lng: -76.74562007188797
    }
  },
  {
    name: 'By Mona School of Business',
    faculty: 'MSBM',
    feature: 'parking',
    coords: {
      lat: 18.0082994881158,
      lng: -76.74677342176437
    }
  },
  {
    name: 'By Law',
    faculty: 'Law',
    feature: 'parking',
    coords: {
      lat: 18.00847294326504,
      lng: -76.74743860960007
    }
  },
  {
    name: 'By Med',
    faculty: 'MedSci',
    feature: 'parking',
    coords: {
      lat: -76.74743860960007,
      lng: -76.74717307090759
    }
  },
  {
    name: 'By Main Library',
    feature: 'parking',
    coords: {
      lat: 18.005973131970315,
      lng: -76.7453545331955
    }
  },
  {
    name: 'Juici Patties',
    feature: 'food',
    coords: {
      lat: 18.004942587198343,
      lng: -76.74857318401337
    }
  },
  {
    name: 'KFC',
    feature: 'food',
    coords: {
      lat: 18.006151691095663,
      lng: -76.74472153186798
    }
  },
  {
    name: 'Burger King',
    feature: 'food',
    coords: {
      lat: 18.008273979991213,
      lng: -76.74730181694031
    }
  },
  {
    name: 'Beehive',
    feature: 'food',
    coords: {
      lat: 18.004544652456293,
      lng: -76.74638450145721
    }
  },
  {
    name: 'Spot',
    feature: 'food',
    coords: {
      lat: 18.000738718394295,
      lng: -76.74346089363098
    }
  },
];
