import { HomeItem } from '../models/models';

export const HOME_ITEMS: HomeItem[] = [
  {
    title: 'School Map',
    icon: 'assets/images/map.svg',
    link: '/home/map',
    isVisible: true,
    forStudentOnly: false,
  },
  // {
  //   title: 'Bus Schedule',
  //   icon: 'assets/images/schedule.svg',
  //   link: '/home/busses',
  //   isVisible: true
  // },
  // {
  //   title: 'Food Places',
  //   icon: 'assets/images/restaurant.svg',
  //   link: '/home/restaurants',
  //   isVisible: true
  // },
  {
    title: 'Schedule',
    icon: 'assets/images/newspaper.svg',
    link: '/home/schedule',
    isVisible: true,
    forStudentOnly: true
  },
  {
    title: 'Parking',
    icon: 'assets/images/parked-car.svg',
    link: '/home/parking',
    isVisible: true,
    forStudentOnly: false
  },
  // {
    //   title: 'Exam',
    //   icon: 'calendar',
  //   link: '/home/schedule',
  //   isVisible: true
  // },
  {
    title: 'News',
    icon: 'assets/images/newspaper.svg',
    link: '/home/news-and-events',
    isVisible: true,
    forStudentOnly: false
  },
  {
    title: 'Campus Contacts',
    icon: 'assets/images/phonebook.svg',
    link: '/home/directory',
    isVisible: true,
    forStudentOnly: false
  }
];
