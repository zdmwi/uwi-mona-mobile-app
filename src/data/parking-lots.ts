import { ParkingLot } from './../models/models';

export const PARKING_LOTS: ParkingLot[] = [
  {
    name: "Behind Sci-Tech",
    img_url: 'https://dummyimage.com/400x400/c9bfbf/000000',
    coords: {
      lat: 18.003572,
      lng: -76.749912
    }
  },
  {
    name: "By Juici Patties",
    img_url: 'https://dummyimage.com/400x400/c9bfbf/000000',
    coords: {
      lat: 18.005566,
      lng: -76.748634
    }
  },
  {
    name: "Main Car Park",
    img_url: 'https://dummyimage.com/400x400/c9bfbf/000000',
    coords: {
      lat: 18.002201,
      lng: -76.745864
    }
  },
  {
    name: "By Medical Sciences",
    img_url: 'https://dummyimage.com/400x400/c9bfbf/000000',
    coords: {
      lat: 18.008904,
      lng: -76.747532
    }
  },
  {
    name: "By MSBM North",
    img_url: 'https://dummyimage.com/400x400/c9bfbf/000000',
    coords: {
      lat: 18.008537,
      lng: -76.747023
    }
  },//change the coordinates for the ones after this
  {
    name: "By Taylor Hall",
    img_url: 'https://dummyimage.com/400x400/c9bfbf/000000',
    coords: {
      lat: 18.008904,
      lng: -76.747532
    }
  },
  {
    name: "By Towers",
    img_url: 'https://dummyimage.com/400x400/c9bfbf/000000',
    coords: {
      lat: 18.008537,
      lng: -76.747023
    }
  },
  {
    name: "By Chancellor Hall",
    img_url: 'https://dummyimage.com/400x400/c9bfbf/000000',
    coords: {
      lat: 18.008904,
      lng: -76.747532
    }
  },
  {
    name: "By Mary Seacole Hall",
    img_url: 'https://dummyimage.com/400x400/c9bfbf/000000',
    coords: {
      lat: 18.008537,
      lng: -76.747023
    }
  },
];
