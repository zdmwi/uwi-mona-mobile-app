# UWI Mobile App

Written in [Angular](https://angular.io) using [Ionic](https://ionicframework.com/docs/intro)


## Setup
To install ionic run the following command in a terminal
```
npm i -g ionic
```
Run the following command to download all of the project's dependencies.
```bash
npm install
```

## Serving the app
```bash
ionic serve
```
Add the --devapp flag to be able to test the app on your device using the [ionic devapp](https://ionicframework.com/docs/appflow/devapp)

## Building for android
### Dependencies

Before building for android, you will need to have the following dependencies installed locally:
+ [Java](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
+ [Gradle](https://gradle.org/install/)
+ [Android Studio](https://developer.android.com/studio/), which includes the android sdk

### Build Commands
Before you can build you need to add the android platform to your project. Do so by running the following command:
```bash
ionic cordova add android
```

To create a dev build run:
```bash
ionic cordova build android
```
To create a staging/production build run:
```bash
ionic cordova build --configuration=[staging/production]
```

## Generate Spash Screens and Icons

These are Cordova resources. You can replace icon.png and splash.png and run
`ionic cordova resources` to generate custom icons and splash screens for your
app. See `ionic cordova resources --help` for details.

Cordova reference documentation:

- Icons: https://cordova.apache.org/docs/en/latest/config_ref/images.html
- Splash Screens: https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-splashscreen/

